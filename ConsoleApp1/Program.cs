﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1 {
	class Program {
		static void Main(string[] args) {

			int[] arr = new int[] { 85, 1, 5, 7, 3, 34, 85, 23, 74, 98, 2, 6 };
			var list = new List<int>();

			foreach (int a in arr)
				if (a > 14)
					list.Add(a);

			list.Sort((a, b) => b.CompareTo(a));

			foreach (var a in list)
				Console.WriteLine(a);

			Console.ReadLine();
		}
	}
}
